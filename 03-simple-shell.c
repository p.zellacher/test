#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main()
{
  //50 Byte fuer Befehlstring
  char kdo[50];
  //Prozess ID
  pid_t pid;
  int status;

  while(1){
    printf(">");
    scanf("%s", &kdo);

    //Kinder-Prozess erzeugen
    pid = fork();

    //Pruefen, ob der Kinder-Prozess (dieser ist gleich 0)
    if(pid == 0){
      printf("Process-ID: %d\n", getpid());
      //Ueberschreiben des Prozesses mit dem neuen Befehl
      execl(kdo,kdo,NULL);
    } else {
      printf("Process-ID: %d\n", getpid());
      wait(&status);
    }
  }
}
